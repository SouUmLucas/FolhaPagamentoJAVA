package folhapagamento;

import java.util.ArrayList;

public class Empresa {
    private String cnpj, razaoSocial, nomeFantasia;
    ArrayList<Departamento> departamentos = new ArrayList();
    
    public Empresa(String cnpj, String razaoSocial, String nomeFantasia) {
        this.cnpj = cnpj;
        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }
    
    public void AdicionarDepartamentos(Departamento departamento) {
        this.departamentos.add(departamento);
    }
}
