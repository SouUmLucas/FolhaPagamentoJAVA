package folhapagamento;

public class FolhaPagamento {
    public static void main(String[] args) {
        Empresa emp = new Empresa("01111", "Giter LTDA", "Giter");
        
        Departamento dep1 = new Departamento("Logistica");
        Departamento dep2 = new Departamento("RH");
        Departamento dep3 = new Departamento("Engenharia");
        
        emp.AdicionarDepartamentos(dep1);
        emp.AdicionarDepartamentos(dep2);
        emp.AdicionarDepartamentos(dep3);
        
        
    }    
}
